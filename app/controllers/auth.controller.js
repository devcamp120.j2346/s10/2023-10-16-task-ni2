const db = require("./../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const registerUser = async (req,res) => {
    try {
        const existingUser = await db.user.find({
            username:  req.body.username
        });

        if (existingUser) {
            return res.status(400).json({
                message: "User exists"
            })
        }

        const user = await db.user.create({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 2)
        })

        return res.json(user);
    } catch (e) {
        console.error(e);
        process.exit();
    }
}

const login = async (req,res) => {
    try {
        if (!req.body.password) {
            res.status(400).json({
                message: "Password is required."
            })
        }

        if (!req.body.username) {
            res.status(400).json({
                message: "Username is required."
            })
        }

        const existingUser = await db.user.find({
            username:  req.body.username
        });


        if (!existingUser) {
            return res.status(400).json({
                message: "User doesn't exists."
            })
        }    

        const isValid = bcrypt.compareSync(req.body.password, existingUser[0].password);

        if (!isValid) {
            return res.status(401).json({
                message: "Credential is invalid"
            })
        }

        const token = jwt.sign({
            sub: existingUser[0]._id,
            username: existingUser[0].username
        }, "secret", {
            algorithm: "HS256",
            allowInsecureKeySizes: true,
            expiresIn: 60*60*24
        })

        res.json({
            token: token
        });
    } catch (e) {
        console.error(e);
        process.exit();
    }
}

module.exports = {registerUser, login}