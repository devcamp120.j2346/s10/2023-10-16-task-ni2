
const mongoose = require("mongoose");

const db = {};

db.user = require("./user.model");
db.mongoose = mongoose;

module.exports = db;