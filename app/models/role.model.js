const mongoose = require("mongoose");

const roleModel = mongoose.Schema({
    name: {
        type: String,
        unique: true,
        require: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Role", roleModel);