const mongoose = require("mongoose");

const userModel = mongoose.Schema({
    username: {
        type: String,
        unique: true,
        require: true
    },
    password: {
        type: String,    
        require: true
    },
    roles: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Role"
        }
    ]
}, {
    timestamps: true
})

module.exports = mongoose.model("User", userModel);