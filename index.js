const express = require("express");
const cors = require("cors");
const db = require("./app/models");
const authRoute = require("./app/routers/auth.route");
const dotenv = require("dotenv");

if (process.env.NODE_APP.trim() === 'development') {
    dotenv.config({path: "./.env.development"});
}

const app = express();
app.use(cors());
app.use(express.json());

db.mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`).then(() => {
    console.log("DB connected successufully");    
}).catch((e) => {
    console.error(e);
    process.exit();
});

app.get("/", (req, res) => {
    res.json({message: "Hello world"});
})

app.use("/api/auth", authRoute);

app.listen(process.env.APP_PORT, () => {
    console.log("App is running at port " + process.env.APP_PORT);
});